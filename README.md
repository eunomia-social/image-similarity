## Environment variables

- IMAGE_SIMILARITY_PORT=5055
- IMAGE_SIMILARITY_TAG=latest
## Usage:
- Image Embedding:
  - HTTP method: POST
  - url: http://eunomia-image-similarity:5055/image_similarity_vector
  - application/json input: {"image_URL": "https://example.com/an/imge.png"}
  - json output: {"result": <image_feature_vector>} <br>
  where:
    - <image_feature_vector> is a list of list of floats, containing the specified image's feature vector. 

- Similarity:
  - HTTP method: POST
  - url: http://eunomia-image-similarity:5055/image_similarity_batch
  - application/json input:
    ```
      {
        "image_feature_vector1": "[[...],[...],...[...]]",
        "image_feature_vector2": "[[...],[...],...[...]]"
      }
    ```
  - json output: {"result": <image_similarity_batch>} <br>
  where:
    - <image_similarity_batch> is for a batch of image similarity results (a float between 0 and 1), indicating the computed similarities between the supplied feature vector of a new image and the exsiting images.
