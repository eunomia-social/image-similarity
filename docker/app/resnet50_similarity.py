# -*- coding: utf-8 -*-
import numpy as np
from app.resnet50 import ResNet50
from keras.layers import Input
from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input
from sklearn.metrics.pairwise import cosine_similarity
import multiprocessing
from urllib.request import urlopen
from io import BytesIO

try:
    from app.resnet50 import WEIGHTS_PATH_NO_TOP
except ImportError:
    import sys
    import os
    sys.path.append(os.path.realpath(os.path.dirname(__file__)))
    from app.resnet50 import  WEIGHTS_PATH_NO_TOP  # noqa

    
def separate_process(func):
    def parallel_wrapper(output_dict, *argv, **kwargs):
        ret = func(*argv, **kwargs)
        if ret is not None:
            output_dict["ret"] = ret
    def outer_wrapper(*argv, **kwargs):
        same_process = kwargs.pop("same_process", False)
        if same_process:
            return func(*argv, **kwargs)
        with multiprocessing.Manager() as manager:
            output = manager.dict()
            args = (output,) + argv
            p = multiprocessing.Process(
                target=parallel_wrapper, args=args, kwargs=kwargs
            )
            p.start()
            p.join()
            ret_val = output.get("ret", None)
        return ret_val
    return outer_wrapper


# Generate image feature vector from image URL
class Image_Similarity_Feature_Generator:
    def __init__(self, URL):
        self.image_URL = URL
    @separate_process
    def feature_generation(self):
        # Read an image from URL,then load and preprocess the image  
        with urlopen(self.image_URL) as url:
            img = image.load_img(BytesIO(url.read()), target_size=(224, 224, 3))
        preprocessed_img = preprocess_input(
            np.expand_dims(image.img_to_array(img), axis=0)
        )
        # Build ResNet50 feature_model as an image feature extractor
        feature_model = ResNet50(input_tensor=Input(shape=(224, 224, 3)), include_top=False, weights='imagenet')
        feature_model.load_weights(WEIGHTS_PATH_NO_TOP + '/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5')        
        feature_vector = feature_model.predict(preprocessed_img)
        a,b,c,n = feature_vector.shape
        feature_vector= feature_vector.reshape(1,n)
        assert feature_vector.shape == (1, 2048)
        return feature_vector


# Predict similarity between two images from their feature vectors
class Image_Similarity_Prediction:
    def __init__(self, image_feature_vector1, image_feature_vector2):
        self.image_feature_vector1 = image_feature_vector1
        self.image_feature_vector2 = image_feature_vector2
    @separate_process
    def calculate_cosine_similarity(self):
        return cosine_similarity(self.image_feature_vector1, self.image_feature_vector2)


# Predict image similarity in a batch
class Image_Similarity_Prediction_Batch:
    """
    Process Image Similarity In a Batch.
    input: 
        mage_feature_vector_pairs:
         [
          [[[new_post_image_vector]],[[exsit_post_1_image_vector]]],  
          [[[new_post_image_vector]],[[exsit_post_2_image_vector]]], 
          ...
          [[[new_post_image_vector]],[[exsit_post_n-1_image_vector]]],
          [[[new_post_image_vector]],[[exsit_post_n_image_vector]]]
         ] 
    output:
        image_similarity_results: 
        [1.0000000000000007, 0.17456336068839245, 
         ...
         0.1304673416244404, 0.14011167748515213]
    """
    def __init__(self, image_feature_vector_pairs):
        self.image_feature_vector_pairs = image_feature_vector_pairs              
    @separate_process
    def caculate_image_vector_pairs_similarity(self): 
        image_similarity_results =[]
        for vector_pair in self.image_feature_vector_pairs:
            image_similarity_results.append(float(Image_Similarity_Prediction
                                                   (vector_pair[0],vector_pair[1]).
                                                   calculate_cosine_similarity()[0][0]))
        return image_similarity_results  
    
