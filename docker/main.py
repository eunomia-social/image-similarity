"""-*- coding: utf-8 -*-.
Image Similarity - FastAPI routing
"""
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel, HttpUrl
from typing import List, Union
import numpy as np  # noqa

#Disable the warning "Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA" (27/08/2021)
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

try:
    from app.resnet50_similarity import (
        Image_Similarity_Feature_Generator,
        Image_Similarity_Prediction_Batch,
    )
    
except ImportError:
    import sys
    import os

    sys.path.append(os.path.realpath(os.path.dirname(__file__)))
    from app.resnet50_similarity import (  # noqa
        Image_Similarity_Feature_Generator,  # noqa
        Image_Similarity_Prediction_Batch,  # noqa
    )
        
app = FastAPI(redoc_url="/redoc", docs_url="/docs")


@app.head("/health", include_in_schema=False, status_code=204)
async def head_check():  # pragma: no cover
    """Health check"""
    return None


@app.get("/")
async def index():
    """
    Image feature vector generation example:
        {
         "image_URL": "https://ichef.bbci.co.uk/news/976/cpsprodpb/12A9B/production/_111434467_gettyimages-1143489763.jpg",
       }
       OR
       {
         "image_URL": "https://wallpapercave.com/wp/wp3505734.jpg",
       }

    Image similarity prediction example:
        Firstly, you need to generate two image feature vectors from image URLs using "/image_similarity_vector".
        Then, you could use "/image_similarity" or "/image_similarity_batch" to predict image similarity from the feature
        vectors genetated.

    """
    return {"Message": "This is image similarity integration test."}


class RequestBodyURL(BaseModel):
    image_URL: HttpUrl


class RequestBodyFeatureVector(BaseModel):
    image_feature_vector_pairs: List[List[Union[List[List[float]],List[List[float]]]]]
    #image_feature_vector1: List[List[float]]
    #image_feature_vector2: List[List[float]]
    

@app.post("/image_similarity_vector")
async def eunomia_image_feature_vector(post: RequestBodyURL):
    """
    Image similarity feature vector generation.

    Returns the image feature vector with the size of (1, 2048) of a post.

    """
    try:
        return {
            "result": Image_Similarity_Feature_Generator(post.image_URL)
            .feature_generation()
            .tolist()
        }
    except Exception as e:
        raise HTTPException(status_code=400, detail={"message": repr(e)})

        
# Batch Process         
@app.post("/image_similarity_batch")
async def eunomia_image_similarity(post: RequestBodyFeatureVector):
    """
    Image similarity analysis handler.

    Returns the image similarity lists (float [0,1]) between a new post and a batch of old exsiting posts (from feature vector       inputs).
    """        
    try:
        return {
            "result":Image_Similarity_Prediction_Batch(np.array(post.image_feature_vector_pairs)).
            caculate_image_vector_pairs_similarity()
        }
    except Exception as e:
        raise HTTPException(status_code=400, detail={"message": repr(e)})
        
        
# Single Process
'''@app.post("/image_similarity")
async def eunomia_image_similarity(post: RequestBodyFeatureVector):
    """
    Image similarity analysis post handler.

    Returns the image similarity (float [0,1]) between two posts (from feature vector inputs).

    """
    try:
        return {
            "result": float(
                Image_Similarity_Prediction(
                    np.array(post.image_feature_vector1),
                    np.array(post.image_feature_vector2),
                ).calculate_cosine_similarity()[0][0],
            )
        }
    except Exception as e:
        raise HTTPException(status_code=400, detail={"message": repr(e)})
'''
