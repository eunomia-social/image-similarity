# -*- coding: utf-8 -*-
"""
Image Similarity - start the service
"""
import uvicorn
import nest_asyncio

nest_asyncio.apply()
if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=5055)
